/* functions */
function format_number(number) {
	//alert(number);
	var num = number.toString().replace(/\$|\,/g,'');	 
	if(isNaN(num))  num = "";
	var sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	num = Math.floor(num/100).toString();
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
	}
	return (((sign)?'':'-') + num);	        

}
/* htabs */
$.fn.htabs = function() {
    var selector = this;
    hash = window.location.hash;
    
    this.each(function() {
            var obj = $(this); 
            $(obj.attr('href')).hide();

            $(obj).click(function() {
                $(selector).removeClass('selected');

                $(selector).each(function(i, element) {
                        $($(element).attr('href')).hide();
                });

                $(this).addClass('selected');

                $($(this).attr('href')).show();

                return false;
            });
    });    
    $(this).first().click();
    
    $(this).show();
    /*
    if(hash == '')
    {
        $(this).first().click();
    }
    else
    {
        $(hash).show();
        $(hash).parents().show();
    }*/
};
$.fn.ajaxtabs = function(options){
    var default_options = {
        content_wrapper : '#ajax-content-wrapper',
        data : {}
    }; 
    
    options = $.extend({},default_options, options);

    var selector = this;
    

    this.each(function() {
            var obj = $(this);            

            $(obj).click(function() {
                $(selector).removeClass('selected');

                $(this).addClass('selected');                
                $(options.content_wrapper).load($(this).attr('href'), options.data, function(){
                    
                });                

                return false;
            });
    });
    $(this).first().click();
    
    $(this).show();
}
/* shuffle */
$.fn.shuffle = function() {
 
    var allElems = this.get(),
        getRandom = function(max) {
            return Math.floor(Math.random() * max);
        },
        shuffled = $.map(allElems, function(){
            var random = getRandom(allElems.length),
                randEl = $(allElems[random]).clone(true)[0];
            allElems.splice(random, 1);
            return randEl;
        });

    this.each(function(i){
        $(this).replaceWith($(shuffled[i]));
    });

    return $(shuffled); 
};

$.fn.dropdownmenu = function(settings) {
    var default_settings = {
        'children' : 'ul',
        'auto_append' : false,        
    };    
    var final_settings = $.extend(default_settings,settings); 
    var $ele = $(this);
    // responsive menu
    if (window.innerWidth < 768) {        
    }else{
        $ele.find("li").hover(function(){                        
                $(this).addClass('current');                        
                $(this).children(final_settings.children).slideDown('fast');
            },function(){                
                $(this).removeClass('current');
                $(this).children(final_settings.children).slideUp('fast');
        });
        $ele.find("li:has(" + final_settings.children + ")").addClass('nav-item-arrow'); //.append('<span style="text-indent:5px;float:right;">&raquo;</span>');    
        if(final_settings.auto_append){
            $ele.find("li.nav-item-arrow > a").prepend('<span style="text-indent:5px;float:right;">&raquo;</span>');
        }
    }   
}

$.fn.fixposition = function(settings) {
    var $ele = $(this);
    var default_settings = {
        'top' : $ele.offset().top,
        'width' : $ele.width(),
    };    
    var final_settings = $.extend(default_settings,settings); 
    
    $(window).scroll(function () {
        if ($(this).scrollTop() > final_settings.top) {
            $ele.css({ 'position': 'fixed', 'top' : 0, 'width' : final_settings.width });
        } else {
            $ele.css({ 'position': 'relative' }); 
        }
    });    
}