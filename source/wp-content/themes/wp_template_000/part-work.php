<div class="primary-row clearfix">
    <h2 class="h2_title">向井組の最新施工事例</h2>
        <table class="top-custom-post-works">
            <colgroup>
                <col style="width:20px;" />
                <col style="width:100px;" />
                <col style="width:200px;" />
            </colgroup>
			
			<?php
	    $queried_object = get_queried_object();	    
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> 'work',
	        'posts_per_page' => get_query_var('posts_per_page'),
	        'paged' => get_query_var('paged'),
	    ));
	    ?>
	    <?php $i = 0; ?>
		<?php foreach($posts as $p) :  ?>	        
			<tr>
				<td><i class="fa fa-play-circle"></i></td>
				<td><?php echo get_the_date('Y.m.d', $p->ID); ?></td>																
				<td><div class="category"><?php @the_terms($p->ID, 'cat-work'); ?></div></td>			
				<td><a href="<?php echo get_the_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></td>				
			</tr>		            
	    <?php endforeach; ?>	    
	    <div class="primary-row voice-page">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    <?php wp_reset_query(); ?>	                                    
        </table>
</div><!-- end primary-row -->