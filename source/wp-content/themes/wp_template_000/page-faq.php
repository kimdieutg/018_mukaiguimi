<?php get_header(); ?>
<p>お客様からよくいただく質問内容をまとめました。</p>
<div class="primary-row clearfix">	
	<div class="faq-content-list">
		<div class="faq-content-row clearfix">
			<div class="question">お見積もりは無料ですか？</div>
			<div class="answer">
				もちろん無料です。 <br />
				造成工事・宅地造成・解体工事の調査などお気軽にご相談ください。
			</div>
		</div><!-- end faq-row1-->
		<div class="faq-content-row clearfix">
			<div class="question">自宅の駐車スペースを舗装したいのですが</div>
			<div class="answer">
				コンクリート土間打ち、アスファルト、インターロッキングなどでの舗装工事も承っております。<br />
				まずはお気軽にご相談ください。
			</div>
		</div><!-- end faq-row2 -->		
		<div class="faq-content-row clearfix">
			<div class="question">田畑を駐車場にしたいのですが</div>
			<div class="answer">
				状況にもよりますが、擁壁などで仕切りを設け、造成工事を行ない十分な締め固めを行なった後、
				舗装工事を行ないます。お見積りは無料ですのでぜひ一度ご相談ください。
			</div>
		</div><!-- end faq-row3 -->	
		<div class="faq-content-row clearfix">
			<div class="question">小さな工事でも対応してもらえますか？</div>
			<div class="answer">
				もちろんお任せください！<br />
				工事の大小に関わらず対応いたします。
			</div>
		</div><!-- end faq-row4 -->
		<div class="faq-content-row clearfix">
			<div class="question">地域の住民に対して、事前の説明会などは行っていただけますか？</div>
			<div class="answer">
				近隣の皆様には、着工前にご挨拶に伺い、工事内容・期間のご説明をさせていただいておりますのでご安心ください。

			</div>
		</div><!-- end faq-row5 -->
		<div class="faq-content-row clearfix">
			<div class="question">施工可能なエリアを教えてください。</div>
			<div class="answer">
				当社では、大阪市を中心に県内全域と和歌山・滋賀など近隣エリアの一部で宅地造成工事を行っています。<br />
				その他のエリアについても対応できますので、まずはご相談ください。
			</div>
		</div><!-- end faq-row6 -->
		<div class="faq-content-row clearfix">
			<div class="question">宅地造成工事規制区域かどうかを知りたいのですが</div>
			<div class="answer">
				宅地造成工事規制区域はその地区によって異なります。<br />
				造成工事を検討中の住所をご連絡いただければお調べいたします。
			</div>
		</div><!-- end faq-row7 -->
		<div class="faq-content-row clearfix">
			<div class="question">擁壁の工事が必要になったのですが</div>
			<div class="answer">
				まずは敷地の状況を詳しくお聞かせください。<br />
				必要であれば現地調査をさせていただきます。<br />
				土留め工事や宅地造成・造成工事もできますのでご安心してご相談ください。
			</div>
		</div><!-- end faq-row8 -->
		<div class="faq-content-row clearfix">
			<div class="question">予算が完全に決まっているのですが。</div>
			<div class="answer">
				はい、予算をご提示いただければ、それに合わせたプランをご提示します。資材なども予算に合わせたものを
				組み合わせて、クオリティはそのままにコストを下げるご提案が可能です。
			</div>
		</div><!-- end faq-row9 -->
	</div>	
</div>

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>