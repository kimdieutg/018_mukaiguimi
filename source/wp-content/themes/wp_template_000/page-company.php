<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h2 class="h2_title">会社概要</h2>
		<table class="company_table">
			<tr>
				<th>会社名</th>
				<td>株式会社 向井組</td>
			</tr>
			<tr>
				<th>代表者</th>
				<td>向井 大志</td>
			</tr>
			<tr>
				<th>住所</th>
				<td>〒598-0033   大阪府泉佐野市南中安松1681-2</td>
			</tr>
			<tr>
				<th>電話番号</th>
				<td>0724-66-3380</td>
			</tr>
			<tr>
				<th>FAX</th>
				<td>0724-66-1282</td>
			</tr>
			<tr>
				<th>URL</th>
				<td>http://www.mukaigumi.com/</td>
			</tr>
			<tr>
				<th>E-mail</th>
				<td>info@mukaigumi.com</td>
			</tr>
			<tr>
				<th>営業時間</th>
				<td>9：00～17：00</td>
			</tr>
			<tr>
				<th>定休日</th>
				<td>日曜・祝日</td>
			</tr>
			<tr>
				<th>業務内容</th>
				<td>土木工事一式</td>
			</tr>
			<tr>
				<th>許認可・資格</th>
				<td>
					【大阪府知事許可】<br/>
					・一般建設業 (般-2) 第75425号<br/>
					・土木工事業 (特-4) 第75425号<br/>
					・とび土木工事業 (特-4) 第75425号<br/>
					・石工事業 (特-4) 第75425号<br/>
					・鋼構造物工事業 (特-4) 第75425号<br/>
					・ほ装工事業 (特-4) 第75425号<br/>
					・しゅんせつ工事業 (特-4) 第75425号<br/>
					・塗装工事業 (特-4) 第75425号<br/>
					・水道施設工事業 (特-4) 第75425号<br/>
					・産業廃棄物収集運搬車　第017442号"
				</td>
			</tr>				
		</table>
	</div>
	<div class="primary-row clearfix">
		<h2 class="h2_title">アクセス</h2>
		<div id="company_map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m20!1m8!1m3!1d3292.2647619088766!2d135.30888199709435!3d34.39461577835608!3m2!1i1024!2i768!4f13.1!4m9!1i0!3e6!4m0!4m5!1s0x6000c83ba9c73f3d%3A0x61ad41c01b5fcb74!2s1681-2+Minaminakayasumatsu%2C+Izumisano-shi%2C+%C5%8Csaka-fu%2C+Japan!3m2!1d34.3946158!2d135.3111887!5e0!3m2!1sen!2sus!4v1412830451265" width="760" height="300" frameborder="0" style="border:0"></iframe>
		</div>
	</div>
<?php get_footer(); ?>