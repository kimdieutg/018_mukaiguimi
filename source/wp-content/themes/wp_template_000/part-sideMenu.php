<div id="sideMenu">
	<ul>
		<li><a href="<?php bloginfo('url'); ?>/"><i class="fa fa-chevron-right"></i>ホーム</a></li>
		<li><a href="<?php bloginfo('url'); ?>/about"><i class="fa fa-chevron-right"></i>宅地造成とは</a></li>
		<li><a href="<?php bloginfo('url'); ?>/business"><i class="fa fa-chevron-right"></i>事業案内</a></li>
		<li><a href="<?php bloginfo('url'); ?>/reliable"><i class="fa fa-chevron-right"></i>3つの安心宣言</a></li>
		<li><a href="<?php bloginfo('url'); ?>/work"><i class="fa fa-chevron-right"></i>施工事例</a></li>
		<li><a href="<?php bloginfo('url'); ?>/voice"><i class="fa fa-chevron-right"></i>お客様の声</a></li>
		<li><a href="<?php bloginfo('url'); ?>/facility"><i class="fa fa-chevron-right"></i>設備紹介</a></li>
		<li><a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-chevron-right"></i>施工までの流れ</a></li>
		<li><a href="<?php bloginfo('url'); ?>/staff"><i class="fa fa-chevron-right"></i>従業員一同</a></li>
		<li><a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-chevron-right"></i>よくあるご質問</a></li>
		<li><a href="<?php bloginfo('url'); ?>/contact"><i class="fa fa-chevron-right"></i>お問い合わせ</a></li>
		<li><a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-chevron-right"></i>会社概要</a></li>
	</ul>
</div>