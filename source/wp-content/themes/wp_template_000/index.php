<?php get_header(); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->
        <h2 class="h2_title">向井組が宅地造成・造成工事で選ばれる理由</h2>        
        <p>
            <img alt="向井組が宅地造成・造成工事で選ばれる理由" src="<?php bloginfo('template_url'); ?>/img/top/top_content_img1.jpg" />
        </p>        
        <div id="top_content1">
            <img alt="top content 1" src="<?php bloginfo('template_url'); ?>/img/top/top_content_img2.jpg" />
            <div class="text">
                確実な技術、きれいな仕上がりが求められる現場等、高度な技術が宅地造成・造成技術に求められます。<br/>
                向井組では経験豊富な人材と確かな実績からお客様の信頼を得ております。<br/>
                向井組があなたのお悩みを解決します。
            </div>
        </div>
        <p class="mt10">
            <img alt="top content 3" src="<?php bloginfo('template_url'); ?>/img/top/top_content_img3.jpg" />
        </p>
    </div><!-- end primary-row -->    
    <div class="primary-row clearfix"><!-- begin primary-row -->
        <h2 class="h2_title">遊休地があるなら、有効な土地活用しませんか？</h2>
        <p>
            <img alt="top content 1" src="<?php bloginfo('template_url'); ?>/img/top/top_content_img4.jpg" />
        </p>
        <p class="mt10">
            こんなご要望をお持ちでしたら、ぜひ当社にご相談ください。<br/>
            ご要望に合わせた適切なご提案をしています。<br/>
            宅地造成・擁壁工事のことなら、まずは当社までお気軽にご相談ください。        
        </p>
    </div><!-- end primary-row -->
    <div class="primary-row clearfix">
        <h2 class="h2_title">向井組の事業紹介</h2>
        <div class="message-right message-360 clearfix">
            <div class="image">
                <img alt="top content 1" src="<?php bloginfo('template_url'); ?>/img/top/top_content_img5.jpg" />
            </div>
            <div class="text pr20">
                <div class="message_content_title">造成工事・宅地造成</div>
                <p class="ln2em">
                    山や斜面、溝、窪地などの地形を切り土、盛り土により<br/>
                    平坦に均し、宅地として使用できるようにする工事です。
                </p>
                <p class="mt10 text-right">
                    <a href="<?php bloginfo('url'); ?>/about">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_large_btn.jpg" />
                    </a>
                </p>
            </div>
        </div>
    </div><!-- end primary-row -->
    <div class="primary-row clearfix">        
        <div class="message-group top-message-classic"><!-- begin message-group -->
            <div class="message-row clearfix"><!--message-row -->
                <div class="message-col message-col190">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img51.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        解体工事
                    </div><!-- end title -->
                    <div class="caption">
                        古くなった擁壁や<br/>
                        石積みを主に解体します。<br/>
                        時には家屋からの<br/>
                        解体作業もおこないます。                        
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/business">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>
                    </div><!-- end link -->
                </div><!-- end message-col -->
                <div class="message-col message-col190">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img52.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        舗装工事
                    </div><!-- end title -->
                    <div class="caption">
                        更地にした土地を舗装しま<br/>
                        す。アスファルト舗装、<br/>
                        コンクリート舗装、イン<br/>
                        ターロッキング工事にも<br/>
                        対応いたします。
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/business">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>                        
                    </div><!-- end link -->
                </div><!-- end message-col -->
                <div class="message-col message-col190">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img53.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        駐車場工事
                    </div><!-- end title -->
                    <div class="caption">
                        アスファルト舗装、<br/>
                        車止め設置、ライン引き…。<br/>
                        簡単な補修から、カラー舗<br/>
                        装や再舗装までおまかせく<br/>
                        ださい。                        
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/business">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>                                                
                    </div><!-- end link -->
                </div><!-- end message-col -->
                <div class="message-col message-col190">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img54.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        外構工事
                    </div><!-- end title -->
                    <div class="caption">
                        フェンス、擁壁工事、<br/>
                        ブロック積みから、<br/>
                        カーポート設置などの<br/>
                        エクステリア施工まで、<br/>
                        トータルで対応                        
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/business">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>                                                                        
                    </div><!-- end link -->
                </div><!-- end message-col -->
            </div><!-- end message-row -->
        </div><!-- end message-group -->    
    </div><!-- end primary-row -->
    <div class="primary-row clearfix">
        <h2 class="h2_title">コンテンツ紹介</h2>
        <div class="message-group top-message-classic"><!-- begin message-group -->
            <div class="message-row clearfix"><!--message-row -->
                <div class="message-col message-col240">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img61.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        宅地造成とは
                    </div><!-- end title -->
                    <div class="caption">
                        宅地造成・造成工事という<br/>
                        名称に馴染みのない方向けに、<br/>
                        工事の概要をご説明させて頂きます。                       
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/about">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>
                    </div><!-- end link -->
                </div><!-- end message-col -->
                <div class="message-col message-col240">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img62.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        ３つの安心宣言
                    </div><!-- end title -->
                    <div class="caption">
                        昭和54年の創業という歴史の中で、<br/>
                        お客さまより高いご支持をいただき、<br/>
                        安心しておまかせいただける弊社の<br/>
                        信条をご紹介します。                       
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/reliable">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>
                    </div><!-- end link -->
                </div><!-- end message-col -->
                <div class="message-col message-col240">
                    <div class="image">
                        <img src="<?php bloginfo('template_url'); ?>/img/top/top_content_img63.jpg" alt="message col" />
                    </div><!-- end image -->
                    <div class="title">
                        お客様の声
                    </div><!-- end title -->
                    <div class="caption">
                        過去に施工しましたお客様から<br/>
                        頂いたたご意見やご要望を掲載して<br/>
                        おります。
                    </div><!-- end caption -->
                    <div class="link">
                        <a href="<?php bloginfo('url'); ?>/voice">
                        <img alt="readmore" src="<?php bloginfo('template_url'); ?>/img/common/primary_small_btn.jpg" />
                        </a>
                    </div><!-- end link -->
                </div><!-- end message-col -->                                
            </div><!-- end message-row -->
        </div><!-- end message-group -->        
    </div><!-- end primary-row -->
    <?php get_template_part('part','work'); ?>
    <div class="primary-row clearfix"><!-- begin primary-row -->        
        <?php get_template_part('part','flow'); ?>
        <?php get_template_part('part','contact'); ?>
    </div><!-- end primary-row -->
    <div class="primary-row clearfix"><!-- begin primary-row -->
        <h2 class="h2_title">向井組からのあいさつ</h2>
        <div id="top_message" class="message-left message-340 clearfix">
            <div class="image">
                <img src="<?php bloginfo('template_url'); ?>/img/common/top_message_img.jpg" alt="top message" />
            </div>
            <div class="text">
                <p class="mb10">
                向井組は、本店所在地でもある大阪を中心とし
                和歌山・滋賀にて、宅地造成・造成工事を行っています。
                お客様の期待に応えること・お客様の声に真摯に向き合うことで
                信頼を得てまいりました。
                </p>
                <p class="mb10">
                宅地造成は家づくりの始まりであり、もっとも重要な部分である
                と考えます。眠っていた大切な土地の有効な利用し、
                新たな資産へと変貌する過程は未来への大きな夢や希望を
                感じずにはいられない事業です。
                </p>
                <p>
                宅地造成・造成工事という仕事に携わる私たちの責任もより
                大きな物と感じながら従事させていただいております。            
                </p>
            </div>
        </div>
    </div><!-- end primary-row -->
<?php get_footer(); ?>            