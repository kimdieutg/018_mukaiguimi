<?php get_header(); ?>
<div class="primary-row clearfix">
	<h2 class="h2_title">お客様に、笑顔でご満足いただくための3つの安心宣言</h2>
	<p class="reliable-info first-child">
		<span class="reliable-text-green">お客様の声</span>に向き合い、<span class="reliable-text-red">真撃</span>に対応
	</p>
	<p class="reliable-info">
		<span class="reliable-text-green">多く</span>の積み重ねた<span class="reliable-text-red">施工事例</span>
	</p>
	<p class="reliable-info">
		<span class="reliable-text-green">個人</span>のお客様から<span class="reliable-text-green">公共工事</span>まで<span class="reliable-text-red">どんな工事</span>にでも対応
	</p>	
	<p class="pt30 pt10" >
		向井組は、本店所在地でもある大阪を中心とし和歌山・滋賀にて、宅地造成・造成工事を行っています。<br />
		お客様の期待に応えること・お客様に満足いただけるようにここに3つの安心宣言を行います。
	</p>
</div><!-- end reliable-row1 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">お客様の声に向き合い、真摯に対応  </h2>	
		<div class="reliable-left reliable-358 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/reliable_content_img1.jpg" alt="reliable" />				
			</div>
			<div class="text">
				<p>向井組はお客様から一つ一つのお客様の声に対して、
				真摯に向き合い誠実・迅速に対応します。</p>
				<p class="pt20">そして、改善へ弛まぬ研鑽を重ね、
				お客様にいつも安心して暮らして頂ける環境作りを実践しています。</p>
				<p clss="pt20">その甲斐あって、今ではご近所の方々に、
				「信頼できる設計施工会社」として名をあげていただけるまでになりました。</p>
				<p>長い歴史の中で築き上げた信頼は、向井組のいちばんの誇りです。</p>
			</div>
		</div>	
</div><!-- end reliable-row2 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">お客様の声に向き合い、真摯に対応  </h2>	
		<div class="reliable-left reliable-358 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/reliable_content_img2.jpg" alt="reliable" />				
			</div>
			<div class="text">
				<p>向井組は創業依頼多くのお客様の施工を行ってきました。</p>
				<p>その長年積み重ねた多くの施工と高い技術力をもって、
				依頼に誠心誠意取り組み、多くの信頼を積み重ねてきた結果、
				今現在の向井組があります。</p>
				<p class="pt20">大阪市内のお客様から和歌山・滋賀のお客様と県をまたぎ
				様々な地域での施工実績がございます。</p>
				<p class="pt20">宅地造成・造成工事はもちろんのこと解体工事・舗装工事・
				駐車場工事・外構工事も多数実績がございます。</p>
			</div>
		</div>	
</div><!-- end reliable-row3 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">お客様の声に向き合い、真摯に対応  </h2>	
		<div class="reliable-left reliable-358 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/reliable_content_img3.jpg" alt="reliable" />				
			</div>
			<div class="text">
				<p>向井組のスタッフには、『その道のプロ（資格保有者）』が
				揃っています。そのため個人のお客様から公共事業まで、
				どんな工事でも対応させていただいております。</p>
				<p class="pt20">工事の際は、安全管理・品質管理・設計管理・工程管理、
				近隣対策を行い計画どおり進捗するように万全の体制で臨
				みます。お客様の立場で施工させていただきます。</p>
			</div>
		</div>	
</div><!-- end reliable-row4 -->
	
<div class="primary-row clearfix">
	<p>
		<a href="<?php bloginfo('url'); ?>/work">
			<img src="<?php bloginfo('template_url'); ?>/img/content/work_content_img6.jpg" alt="reliable" />	
		</a>
	</p>
</div>

 <div class="primary-row clearfix"><!-- begin primary-row -->        
        <?php get_template_part('part','flow'); ?>
        <?php get_template_part('part','contact'); ?>
    </div><!-- end primary-row -->
<?php get_footer(); ?>