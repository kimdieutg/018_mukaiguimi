<?php get_header(); ?>
<div class="primary-row clearfix">	
	<p>
		向井組の施工例です。宅地造成・造成工事はもちろん、解体工事・舗装工事・駐車場工事まで、豊富な施工例で住まいをサポートします。
	</p>
	<div class="primary-row clearfix">
		<div id="work-detail-navi">
	       <ul class="work-detail-navi clearfix">
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/造成工事-宅地造成">造成工事・宅地造成</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/解体工事">解体工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/舗装工事">舗装工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/駐車場工事">駐車場工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-work/外構工事">外構工事</a></li>                                                
	        </ul>		
		</div>
	</div>
</div>

<div class="pt20 clearfix"><!-- begin primary-row -->
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<div class="primary-row clearfix">
		<div class="work-detail-content clearfix">
			<div class="work-title">
				<div class="work-title"><?php the_title(); ?></div>
				<div class="work-date"><?php echo get_the_date('Y/m/d', $p->ID); ?></div>
			</div>
			<table class="work-table-image">
				<tr>						
					<td class="work-table-detail">
						<?php if( get_field('image_before') ): ?>
							<img src="<?php the_field('image_before'); ?>" />
						<?php else :?>
							<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
						<?php endif; ?>
					</td>
					<td class="work-table-detail-arrow"><img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_arrow.jpg" alt="work" /></td>
					<td class="work-table-detail">
						<?php if( get_field('image_before') ): ?>
							<img src="<?php the_field('image_after'); ?>" />
						<?php else :?>
							<img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_content_img.jpg" alt="work" />					
						<?php endif; ?>
					</td>
				</tr>										
			</table>							
			<div class="work-text">				
					<table class="work-table-text">
						<tr>					
							<th class="work-table-title">工事種別</th>
							<td><?php @the_terms($p->ID, 'cat-work'); ?></td>
						</tr>
						<tr>
							<th class="work-table-title">施工場所</th>
							<td><?php the_field('place_text'); ?></td>
						</tr>
						<tr>
							<th class="work-table-title">工期</th>
							<td><?php the_field('date_text'); ?></td>
						</tr>					
				</table>
				
				<div class="work-detail-info">
					<p class="pb10"><img src="<?php bloginfo('template_url'); ?>/img/content/work_detail_info.jpg" alt="work" /></p>
					<p><?php the_field('detail_area'); ?></p>
				</div>
			</div>
		</div>
	</div>	

	<div class="primary-row clearfix">	    
		<?php if( get_previous_post() ): ?>		
		<div class="work-prev-text">
			<?php previous_post_link('%link'); ?>					
		</div>		
		<?php endif;?>		
	</div><!-- end primary-row -->
	
	<?php endwhile;endif; ?>
</div><!-- end primary-row -->

	

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>