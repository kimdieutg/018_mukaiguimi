<?php get_header(); ?>
<div id="message-280" class="primary-row clearfix">
    <h2 class="h2_title">宅地造成・造成工事</h2>	
	<div class="message-group">
        <div class="message-row clearfix">
			<div class="message-right message-280 clearfix">
				<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img1.jpg" alt="about"/>					
					</a>
				</div>
				<div class="text">
					<p class="pb10">山や斜面、溝、窪地などの地形を切り土、盛り土により平坦に均し、
					宅地として整備する工事のことです。
					</p>
					ここでいう造成工事とは、宅地造成工事を意味します。<br />
					田や畑、あるいは現況が宅地であれば、その建物を解体し、土地が低い
					場合は、赤土や残土で盛土をし、高い場合は切土や鋤き取りをして残土を
					処分します。
				</div>
			</div>
		</div>
	</div>
	<p class="pt20">
		一般的に宅地造成・造成工事とは、宅地造成工事規制区域内で「宅地以外の土地を宅地にするため、または宅地に
		おいて行う土地の形質の変更」で次の規模以上のものをいいます。
	</p>
	<p class="pt20">
		<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img2.jpg" alt="about" />		
	</p>
	<div class="about-info">
		・切土によって高さが2メートルをこえる崖ができるもの<br />
		・盛土によって高さが1メートルをこえる崖ができるもの<br />
		・切土と盛土を同時に行う場合で高さが２メートルをこえる崖ができるもの<br />
		・高さに関係なく、切土又は盛土をする土地の面積が500平方メートルをこえるもの
	</div>
	<p>
		※宅地とは「農地、採草、森林、道路、公園、河川その他政令で定める公共の用に供する施設の用に供せられて
		いる土地以外の土地」をいいます。したがって駐車場、テニスコート、墓地等も「宅地」として扱われます。

	</p>
</div><!-- end about-row1 -->
	
<div class="primary-row clearfix">
    <h2 class="h2_title">山や斜面の造成工事、土地分割も向井組にお任せください</h2>	
	<p class="pt10">
		山や斜面、溝、窪地などの地形を切り土、盛り土により平坦に均し、宅地として整備します。<br />
		この工事を行うことにより、土地の有効な利用や排水機能を向上させることができるため、自治体が主導で進めている工事
		から個人で所有している土地に対する工事まで様々です。
	</p>
	<div class="about-left about-280 clearfix">
		<div class="image">				
			<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img3.jpg" alt="about" />				
		</div>
		<div class="text">
			<p class="about-text">
			・遊んでいる土地がある。<br />
			・経費を抑えて、土地を有効活用したい。<br />
			・更地にしたいが、建物が建っているので大変。<br />
			・所有する駐車場の舗装が老朽化してきた。<br />
			・所有する駐車場を拡張し、売上を伸ばしたい。
			</p>
			<p>また、こんなご要望をお持ちでしたら、ぜひ当社にご相談ください。<br />
			ご要望に合わせた適切なご提案をしています。<br />
			宅地造成・擁壁工事のことなら、まずは当社までお気軽にご相談ください。</p>
		</div>
	</div>		
</div><!-- end about-row2 -->

<div id="message-280" class="primary-row clearfix">
    <h2 class="h2_title">お客様が快適に暮らすための土地づくりを行います。</h2>	
	<div class="message-group">
        <div class="message-row clearfix">
			<div class="message-left message-280 clearfix">
				<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/about_content_img4.jpg" alt="about"/>					
					</a>
				</div>
				<div class="text">
					<p>宅地造成・造成工事は、土工事、汚水排水、雨水排水、擁壁、水道・ガス・
					電気などの様々な工事、道路舗装などの工程を積み重ねて進められ、事前ぼ
					測量・設計業務が必須となります。</p>
					<p class="pt20">向井組は宅地造成・造成工事をはじめとするあらゆる土木工事に精通し、
					小規模から大規模な宅地造成工事を行うことが可能です。
					</p>
				</div>
			</div>
		</div>
	</div>	
</div><!-- end about-row3 -->

<div class="primary-row about-text-bottom ln2em clearfix">
	造成事業を通じて、地域への細やかな配慮を心掛けるとともに、<br />
	快適な暮らしの環境形成を図るための取り組みを行っています。
</div>

<div class="primary-row clearfix">
    <h2 class="h2_title">宅地造成・造成工事の事例</h2>	
		<?php
		$queried_object = get_queried_object();	 
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> 'work',
	        'posts_per_page' => get_query_var('posts_per_page'),
	        'paged' => get_query_var('paged'),
	    ));		
	    ?>		
	    <div class="message-group clearfix">
	    <?php $i = 0; ?>
	    <?php foreach($posts as $p) :  ?>	  
			<?php $i++; ?>
	        <?php if($i%2 == 1) : ?>
	        <div class="message-row clearfix">
	        <?php endif; ?>			      
				<div class="message-col message-col370">			
					<div class="about-title"><?php echo $p->post_title; ?></div>
					 <div class="image">
						<?php echo get_the_post_thumbnail( $p->ID,'small'); ?>                    			                    
					</div>															
					<p class="pt10 text-right"><?php echo get_the_date('Y/m/d', $p->ID); ?></p>					        	            
				</div>
				<?php if($i%2 == 0 || $i == count($posts) ) : ?>
	        </div>
	        <?php endif; ?>
	    <?php endforeach; ?>
		<div class="primary-row voice-page">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    </div>
	    <?php wp_reset_query(); ?>    		
</div><!-- end primary-row -->

<div class="primary-row clearfix">
	<p>
		<a href="<?php bloginfo('url'); ?>/work">
			<img src="<?php bloginfo('template_url'); ?>/img/content/work_content_img6.jpg" alt="about" />	
		</a>
	</p>
</div>

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>