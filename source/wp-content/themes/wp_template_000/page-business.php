<?php get_header(); ?>
<div class="primary-row clearfix">
	<h2 class="h2_title">宅地造成から解体工事・舗装工事まで土木工事のことなら</h2>
	<p>
		<img src="<?php bloginfo('template_url');?>/img/content/business_content_img1.jpg" alt="business" />		
	</p>
	<p class="pt20">
		昭和54年の創業以来、「限りない向上心」をモットーに、宅地造成・造成工事を中心に駐車場工事や道路などのアスファルト舗装工事、家屋などの解体工事、
		ブロック塀や擁壁などの外構工事まで、丁寧でスピーディーな施工を心掛けております。
	</p>	
</div><!-- end business-row1 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">解体工事</h2>	
		<div class="business-right business-241 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/business_content_img2.jpg" alt="business" />				
			</div>
			<div class="text">
				ビルやマンション、工場、橋梁などの建造物を取り壊すことを工事です。<br />
				建築物の老朽化のためや、災害等での損傷で修理が困難な場合、何らかの理由で
				建物の使用目的がなくなった場合、などの状況により、解体工事が行われます。<br />
				また、解体後の造成工事や舗装工事なども承っておりますので
				お気軽にご相談ください。<br />
				部分解体も対応しております。
			</div>
		</div>	
</div><!-- end business-row2 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">舗装工事</h2>	
		<div class="business-right business-241 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/business_content_img3.jpg" alt="business" />				
			</div>
			<div class="text">
				人や車が快適に通行できるように、また、道路の寿命を伸ばし、騒音や振動を防ぎます。<br />
				道路の割れ・ヒビの補修などの小さな工事も承ります。<br />
				また、遊休地や空地を駐車場にしたいとお考えの方はお気軽にご相談ください。
			</div>
		</div>	
</div><!-- end business-row3 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">駐車場工事</h2>	
		<div class="business-right business-241 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/business_content_img4.jpg" alt="business" />				
			</div>
			<div class="text">
				折角の土地を遊ばせておくのはもったいないと思われませんか？<br />
				駐車場経営はニーズが高く、初期投資も比較的安価で、メリットが高い
				ビジネスの一つです。<br />
				向井組では、アスファルト舗装、コンクリート舗装などの舗装工事をはじめ、
				車止めの設置、ライン引き、自然石アート舗装工事などのご要望にも対応いた
				しております。<br />
				簡単な補修工事からトータル的なご依頼まで、お客様のニーズに合わせて丁寧に
				施工を行っております。
			</div>
		</div>	
</div><!-- end business-row4 -->

<div class="primary-row clearfix">
	<h2 class="h2_title">外構工事</h2>	
		<div class="business-right business-241 clearfix">
			<div class="image">				
				<img src="<?php bloginfo('template_url'); ?>/img/content/business_content_img5.jpg" alt="business" />				
			</div>
			<div class="text">
				住まいの第一印象は玄関から。<br />
				趣味やこだわりを取り入れて、魅力的な外構エクステリアにしてみませんか。<br />
				フェンス設置、擁壁工事、ブロック積み工事から、カーポート設置、サッシ
				取り付け、ウッドデッキ設置、テラスの新設やお庭の工事まで、あらゆる外構
				工事に対応しております。
			</div>
		</div>	
</div><!-- end business-row5 -->

<?php get_template_part('part','work'); ?>
	
<div class="primary-row clearfix">
	<p>
		<a href="<?php bloginfo('url'); ?>/work">
			<img src="<?php bloginfo('template_url'); ?>/img/content/work_content_img6.jpg" alt="business" />	
		</a>
	</p>
</div>

 <div class="primary-row clearfix"><!-- begin primary-row -->        
        <?php get_template_part('part','flow'); ?>
        <?php get_template_part('part','contact'); ?>
    </div><!-- end primary-row -->
<?php get_footer(); ?>