<?php get_header(); ?>
	<p>
		向井組で宅地造成・造成工事、及び解体工事などの施工を行って戴いたお客様の生の声を ご紹介いたします。<br />
		工事後の正直な気持ちを記入して戴くよう、アンケートを実施しており、 お客様から戴いた声を今後の施工に反映し、
		お客様により満足頂けるよう日々精進しております。<br />
		ここでは、お客様から戴いたアンケート一部分掲載しておりますので、じっくりとご覧戴き、 皆様の今後のご参考になれば
		幸いと思っております。
	</p>
	<div class="pt10 clearfix">		
		<?php
	    $queried_object = get_queried_object();	    
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> $queried_object->name,
	        'posts_per_page' => get_query_var('posts_per_page'),
	        'paged' => get_query_var('paged'),
	    ));
	    ?>
	    <?php $i = 0; ?>
		<?php foreach($posts as $p) :  ?>	        
	        <div class="voice-post">
	        	<div class="voice-post-title">
					<div class="text-left"><?php echo $p->post_title; ?></div>
					<div class="text-right">施工箇所： 			
						<?php @the_terms($p->ID, 'cat-voice'); ?>
					</div>					
				</div>				
				<p><?php echo get_the_post_thumbnail( $p->ID,'small'); ?></p>
				<p class="voice-title"><?php echo $p->customer_title; ?></p>
                <div class="voice-text">
					<?php echo $p->customer_text; ?>						                    
                </div>
				<p class="voice-title"><?php echo $p->staff_title; ?></p>
                <div class="voice-text">
					<?php echo $p->staff_text; ?>						                    
                </div>
	        </div>	        
	    <?php endforeach; ?>	    
	    <div class="primary-row voice-page">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    <?php wp_reset_query(); ?>
	</div>

<div class="primary-row clearfix"><!-- begin primary-row -->        
    <?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
</div><!-- end primary-row -->
<?php get_footer(); ?>